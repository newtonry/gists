class Gist < ActiveRecord::Base
  attr_accessible :title, :user_id
  validates :title, :user_id, presence: true

  has_many :favorites

  belongs_to :user
end
