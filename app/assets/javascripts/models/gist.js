GistApp.Models.Gist = Backbone.Model.extend({
	urlRoot: "/gists",

	// favorite: function(id){
	//
	// 	if (!this._favorite){
	// 		this._favorite = new GistApp.Models.Favorite({gist_id: id})
	// 	}
	// 	return this._favorite
	// },

	parse: function(response, options){

		if (response.favorites.length > 0){
			this.favorite = new GistApp.Models.Favorite(response.favorites[0])
		}

		delete response.favorites;
		return response;
	},

	toJSON: function(){

		var json = _.extend({}, this.attributes)
		if (this.favorite){
			json.favorite = this.favorite.toJSON();
		}


		return json;
	}

});
