GistApp.Models.Favorite = Backbone.Model.extend({

	// urlRoot: function(){
	// 	return "/gists/" + this.gist_id + "/favorites"
	// },

	url: function(){
		return "/gists/" + this.gist_id + "/favorite"
	},


	initialize: function(options){
		this.gist_id = options.gist_id;
	}

});
