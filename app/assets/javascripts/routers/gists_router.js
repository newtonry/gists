GistApp.Routers.Gists = Backbone.Router.extend({
	initialize: function($rootEl){
		this.$rootEl = $rootEl;
		this.gists = new GistApp.Collections.Gists;

	},

	routes: {
		'': 'showIndex',
		'gists/new': 'newGist'
	},

	showIndex: function(){
		var that = this;

		var ajaxCallback = {
			success: function(collection, response, options){
				var index = new GistApp.Views.GistsIndex({collection: collection});
				that._switchView(index);

			},
			error: function(collection, response, options){
			}
		}

		this.gists.fetch(ajaxCallback);
	},

	newGist:function(){
		var form = new GistApp.Views.GistForm({
			collection: this.gists
		})
		this._switchView(form);
	},

	_switchView: function(newView){
		if (this.oldView){
			this.oldView.remove();
		}

		this.oldView = newView;
		this.$rootEl.html( newView.render().$el)
	}


});
