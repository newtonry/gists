window.GistApp = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function($rootEl) {

		this.router = new GistApp.Routers.Gists($rootEl)
		Backbone.history.start();
  }
};

$(document).ready(function(){
	var $content = $('#content');
  GistApp.initialize($content);
});
