GistApp.Views.GistDetail = Backbone.View.extend({

	template: JST['gists/detail'],
	el: '<li></li>',

	events: {
		"click .favorite": "addFavorite",
		"click .unfavorite": "removeFavorite"
	},

	render: function() {
		var gist = this.model;

		var status = (this.model.favorite === undefined) ? 'favorite' : 'unfavorite'
		// debugger
		var renderedContent = this.template({gist: gist, status: status});

		this.$el.html(renderedContent);
		return this;
	},

	addFavorite: function(event){
		var gist = this.model;
		var favorite = new GistApp.Models.Favorite({gist_id: gist.id });
		var that = this;

		favorite.save({},{
			success: function(model, response, options){
				$(event.currentTarget).
					removeClass("favorite").
					addClass("unfavorite").
					text('unfavorite')

				gist.favorite = model;
			},
			error: function(model, response, options){
			}
		}
	);
	},

	removeFavorite: function(event){
		var favorite = this.model.favorite
		favorite.destroy({
			success: function(model, response, options){
				$(event.currentTarget).
					removeClass("unfavorite").
					addClass("favorite").
					text('favorite')
			}
		})


	}

});