GistApp.Views.GistsIndex = Backbone.View.extend({

  template: JST['gists/index'],

	render: function(){
		var gists = this.collection;

		var $renderedContent = $(this.template({gists: gists}));
		// var nonjq = this.template({gists: gists});

		this.$el.html($renderedContent);
		var that = this;

		gists.each(function(gist){
			var gistView = new GistApp.Views.GistDetail({model:gist})
			$(that.$el).find('ul').append(gistView.render().$el)
		});



		return this;
	},
});
