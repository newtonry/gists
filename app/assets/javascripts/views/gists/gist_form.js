GistApp.Views.GistForm = Backbone.View.extend({

	template: JST['gists/form'],

	events: {
		"click #submit_button" : "submitForm"
	},

	render: function(){
		var renderedContent = this.template();
		this.$el.html(renderedContent);

		return this;
	},

	submitForm: function(event) {
		event.preventDefault();

		var data = $('form').serializeJSON();
		var gist = new GistApp.Models.Gist(data);
		debugger;

		this.collection.create(data, {
			success: function(model, response, options) {
				GistApp.router.navigate( '/' , {trigger: true})
			},
			error: function(model, response){
				debugger;
			}
		});


	}



});