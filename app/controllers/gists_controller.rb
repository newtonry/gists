class GistsController < ApplicationController
  def index
    @gists = current_user.gists.includes(:favorites)
    puts @gists
    render :json => @gists, :include => :favorites
  end

  def create
    @gist = current_user.gists.new(params[:gist])

    if @gist.save
      render :json => @gist, :include => :favorites, :status => 200
    else
      render :json => @gist.errors.full_messages, :status => 422
    end
  end


end
