class FavoritesController < ApplicationController
  def create
    @favorite = current_user.favorites.new(params[:favorite])

    if @favorite.save
      render :json => @favorite
    else
      render :json => @favorite.errors.full_messages, :status => 422
    end
  end

  def destroy
    @favorite = Favorite.
        find_by_gist_id_and_user_id(
        params[:gist_id], current_user.id)

    if @favorite && @favorite.destroy
      render :json => "Sucessfully destroyed".to_json, :status => 200
    else
      render :json => "Error in destruction".to_json, :status => 422
    end
  end

  def index
    @favorites = current_user.favorites

    render :json => @favorites
  end
end
