NewAuthDemo::Application.routes.draw do
  resources :users, :only => [:create, :new, :show]
  resource :session, :only => [:create, :destroy, :new]
  resources :gists, :only => [:index, :create] do
    # resources :favorites, :only => [:create, :destroy]
    resource :favorite, :only => [:create, :destroy]
  end

  resources :favorites, :only => [:index]
  # get "static_pages/root", as: "root"

  root :to => "static_pages#root"
end
